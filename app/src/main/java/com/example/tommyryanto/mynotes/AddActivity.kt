package com.example.tommyryanto.mynotes

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.FirebaseDatabase
import com.scottyab.aescrypt.AESCrypt
import kotlinx.android.synthetic.main.activity_add.*
import android.content.SharedPreferences
import android.widget.Toast


class AddActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

        addNotes.setOnClickListener {
            var encryptedTitle = AESCrypt.encrypt(resources.getString(R.string.encryptCode), titleNote.text.toString())
            var encryptedContent = AESCrypt.encrypt(resources.getString(R.string.encryptCode), content.text.toString())

            val root = FirebaseDatabase.getInstance().getReference("Notes")
            val prefs = getSharedPreferences("MyPref", 0)
            val username = prefs.getString("username", "no name")
            val userID = root.push().key
            val notes = Notes(userID, encryptedContent, encryptedTitle, username)
            root.child(userID).setValue(notes).addOnCompleteListener {
                Toast.makeText(this, "Notes added" , Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}
