package com.example.tommyryanto.mynotes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.scottyab.aescrypt.AESCrypt
import kotlinx.android.synthetic.main.activity_sign_up.*
import com.google.firebase.database.DatabaseReference



class SignUpActivity : AppCompatActivity() {

    var msg = ""
    var valid = true
    var encryptedUsername = ""
    var encryptedEmail = ""
    var encryptedPassword = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        signupButton.setOnClickListener {
            msg = ""
            valid = true
            val usernameString = username.text.toString()
            val password = password.text.toString()
            val confirmPassword = confirmPassword.text.toString()
            val email = email.text.toString()


            encryptedPassword = AESCrypt.encrypt(resources.getString(R.string.encryptCode), password)
            encryptedEmail = AESCrypt.encrypt(resources.getString(R.string.encryptCode), email)
            encryptedUsername = AESCrypt.encrypt(resources.getString(R.string.encryptCode), usernameString)

            if(password != confirmPassword) {
                valid = false
                msg = "password and confirm password not matched!"
            }

            val root = FirebaseDatabase.getInstance().getReference()
            val reference = root.child("Accounts").orderByChild("username").equalTo(encryptedUsername)

            reference.addListenerForSingleValueEvent(object: ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot.exists()) {
                        //This means the value exist, you could also dataSnaphot.exist()
                        valid = false
                        msg = "Account exists!"
                        error()
                    }else{
                        if(valid){
                            valid()
                        }
                        else{
                            error()
                        }
                    }
                }

                override fun onCancelled(p0: DatabaseError?) {

                }
                //More auto generated code
            })
        }
    }

    fun valid(){
        val ref = FirebaseDatabase.getInstance().getReference("Accounts")
        val accID = ref.push().key
        val accounts = Accounts(accID, encryptedUsername, encryptedPassword, encryptedEmail)
        ref.child(accID).setValue(accounts).addOnCompleteListener {
            Toast.makeText(this, "Account created" + msg, Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    fun error(){
        val alert = AlertDialog.Builder(this).setTitle("Error").setMessage(msg).setNegativeButton("OK", {_,_ -> })
        alert.show()
    }
}
