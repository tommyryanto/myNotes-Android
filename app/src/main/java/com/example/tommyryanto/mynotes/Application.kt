package com.example.tommyryanto.mynotes

import android.app.Application
import io.multimoon.colorful.Defaults
import io.multimoon.colorful.ThemeColor
import io.multimoon.colorful.initColorful

/**
 * Created by tommyryanto on 26/7/18.
 */

class Application: Application() {
    override fun onCreate() {
        super.onCreate()
        val defaults = Defaults(
                primaryColor = ThemeColor.BLACK,
                accentColor = ThemeColor.BLUE,
                useDarkTheme = true,
                translucent = false)
        initColorful(this, defaults)
        print("application called")


    }
}