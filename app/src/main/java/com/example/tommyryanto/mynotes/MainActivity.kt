package com.example.tommyryanto.mynotes

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.scottyab.aescrypt.AESCrypt
import io.multimoon.colorful.*
import kotlinx.android.synthetic.main.activity_main.*
import android.R.id.edit
import android.content.SharedPreferences



class MainActivity : AppCompatActivity() {

    var encryptedUsername = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        Colorful().apply(this, override = true, baseTheme = BaseTheme.THEME_APPCOMPAT)

        signinButton.setOnClickListener {
            val username = username.text.toString()
            val password = password.text.toString()
            encryptedUsername = AESCrypt.encrypt(resources.getString(R.string.encryptCode), username)
            val encryptedPassword = AESCrypt.encrypt(resources.getString(R.string.encryptCode), password)

            val root = FirebaseDatabase.getInstance().getReference()
            val reference = root.child("Accounts").orderByChild("username").equalTo(encryptedUsername)

            reference.addListenerForSingleValueEvent(object: ValueEventListener{
                override fun onCancelled(databaseError: DatabaseError?) {

                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if(snapshot.exists()){
                        //data found
                        var accountID = ""
                        for (childSnapshot in snapshot.getChildren()) {
                            accountID = childSnapshot.getKey()
                        }
                        checkPassowrd(accountID, encryptedPassword)
                    }
                    else{
                        wrongAccount("Password/Username is wrong!")
                    }
                }

            })
        }

        signupButton.setOnClickListener {
            val move = Intent(this, SignUpActivity::class.java)
            startActivity(move)
        }
    }

    fun checkPassowrd(accountID : String, encryptedPassword: String){
        val root = FirebaseDatabase.getInstance().getReference()
        val passwordReference = root.child("Accounts").child(accountID).child("password")
        passwordReference.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot2: DataSnapshot){
                if(snapshot2.value.toString() == encryptedPassword){
                    accountFound()
                }
                else{
                    wrongAccount("Password/Username is wrong!")
                }
            }

            override fun onCancelled(var1: DatabaseError){}
        })
    }

    fun wrongAccount(check: String){
        val alert = AlertDialog.Builder(this).setTitle("Error").setMessage("Username or password is wrong!"+check)
                .setNegativeButton("OK", {dialogInterface, i ->
                    Toast.makeText(applicationContext, "Re-input the username and password!", Toast.LENGTH_SHORT).show()
                })
        alert.show()
    }

    fun accountFound(){
        Toast.makeText(this, "Account Found!", Toast.LENGTH_SHORT).show()

        val pref = applicationContext.getSharedPreferences("MyPref", 0) // 0 - for private mode
        val editor = pref.edit()

        editor.putString("username", encryptedUsername)
        editor.apply()

        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
    }

}
