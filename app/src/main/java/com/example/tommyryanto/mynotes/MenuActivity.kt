package com.example.tommyryanto.mynotes

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu.*
import android.R.id.edit
import android.content.SharedPreferences
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import com.example.tommyryanto.mynotes.Adapter.NotesAdapter
import com.google.firebase.database.*
import com.google.firebase.database.GenericTypeIndicator
import com.scottyab.aescrypt.AESCrypt
import java.security.GeneralSecurityException


class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)



        addNotes.setOnClickListener {
            val intent = Intent(this, AddActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()

        retrieveData()
        //var notes = Notes("asada", "Asdad","dsfsfd", "dsfsfs")
        //models.add(notes)
//        val notesAdapter = NotesAdapter(this@MenuActivity, models)
//        listItem.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
//        listItem.setHasFixedSize(true)
//        listItem.adapter = notesAdapter
    }

    fun retrieveData(){
        var models = ArrayList<Notes>()
        val prefs = getSharedPreferences("MyPref", 0)
        val username = prefs.getString("username", "no name")
        val databaseNotes = FirebaseDatabase.getInstance().reference.child("Notes").orderByChild("username").equalTo(username)


        databaseNotes.addListenerForSingleValueEvent(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                models.clear()

                val items = dataSnapshot!!.children.iterator()

                while(items.hasNext()){
                    val currentItem = items.next()

                    val map = currentItem.getValue() as HashMap<String, Any>
                    var contentEncrypt = map!!.get("content") as String
                    var titleEncrypt = map!!.get("title") as String
                    var usernameEncrypt = map!!.get("username") as String
                    try {
                        var contentDecrypted = AESCrypt.decrypt(resources.getString(R.string.encryptCode), contentEncrypt)
                        var titleDecrypted = AESCrypt.decrypt(resources.getString(R.string.encryptCode), titleEncrypt)
                        var usernameDecrypted = AESCrypt.decrypt(resources.getString(R.string.encryptCode), usernameEncrypt)
                        var notes = Notes(currentItem.key, contentDecrypted, titleDecrypted, usernameDecrypted)
                        models.add(notes)
                    }catch(e: GeneralSecurityException){

                    }
                }

                val notesAdapter = NotesAdapter(this@MenuActivity, models)
                listItem.layoutManager = LinearLayoutManager(this@MenuActivity, LinearLayout.VERTICAL, false)
                listItem.setHasFixedSize(true)
                listItem.adapter = notesAdapter
                //var notesData: Notes? = dataSnapshot!!.getValue(Notes::class.java)
                //models.add(notesData!!)
            }
        })
    }
}
