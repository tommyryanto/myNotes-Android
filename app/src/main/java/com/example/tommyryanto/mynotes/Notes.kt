package com.example.tommyryanto.mynotes

data class Notes(val id: String,
            val content: String,
            val title: String,
            val username: String)