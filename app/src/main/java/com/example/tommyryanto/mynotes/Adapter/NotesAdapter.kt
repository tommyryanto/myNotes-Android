package com.example.tommyryanto.mynotes.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.tommyryanto.mynotes.*
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.card_items.view.*

class NotesAdapter(val context: Context, val models: ArrayList<Notes>): RecyclerView.Adapter<NotesAdapter.MyHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesAdapter.MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_items, parent, false)

        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: NotesAdapter.MyHolder, position: Int) {
        holder.bindItems(models[position])
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("title", models[position].title)
            intent.putExtra("content", models[position].content)
            context.startActivity(intent)
        }
        holder.delete(models[position], context)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bindItems(note: Notes){
            itemView.title.text = note.title
        }
        fun delete(note: Notes, context: Context){
            itemView.remove.setOnClickListener {
                val db = FirebaseDatabase.getInstance().reference.root.child("Notes")
                db.child(note.id).removeValue()
                Toast.makeText(context, "Notes removed", Toast.LENGTH_SHORT).show()
                (context as MenuActivity).retrieveData()
            }
        }
    }
}